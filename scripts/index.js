const cardState = {
  toggle: false,
  0: { 
    h1: "Gustavo Ilha Morais", 
    p: "Software Engineer" ,
    i: "fa-sharp fa-solid fa-circle-up"
  },
  1: {
    h1: "Find me",
    p: `
        <a href="https://github.com/gustavoilhamorais" target="_blank">
          <i class="fa-brands fa-github-alt"></i>
        </a>
        <a href="https://gitlab.com/gustavoilhamorais" target="_blank">
          <i class="fa-brands fa-gitlab"></i>
        </a>
        <a href="https://www.linkedin.com/in/gustavo-ilha-morais-283b19161" target="_blank">
          <i class="fa-brands fa-linkedin"></i>
        </a>
        <a href="https://www.instagram.com/gustavoilhamorais" target="_blank">
          <i class="fa-brands fa-instagram"></i>
        </a>
        <a href="https://twitter.com/ilhamorais" target="_blank">
          <i class="fa-brands fa-twitter"></i>
        </a>
        `,
    i: "fa-solid fa-circle-down"
  }
}

const toggle = () => cardState.toggle = !cardState.toggle;

const updateDomContent = (tag = "h1" || "p" || "i") => {
  document.querySelector(tag)[{ h1: "innerText", p: "innerHTML", i: "className" }[tag]] = cardState[Number(cardState.toggle)][tag];
}

function cardClick() {
  updateDomContent("h1");
  updateDomContent("p");
  updateDomContent("i");
  toggle();
}

const handleResponsivity = (innerWidth = window.innerWidth) => {
  const card = document.querySelector(".card");
  if (innerWidth > 1000 && innerWidth < 1800) card.className = "card mtt-5";
  else if (innerWidth > 1800) card.className                 = "card mtt-4";
  else card.className                                        = "card mt-5";
}

const onLoad = () => {
  window.addEventListener("resize", handleResponsivity);
  handleResponsivity();
  cardClick();  
}

onLoad();